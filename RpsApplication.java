// Mohammad Khan, 2038700
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	

    private RpsGame game = new RpsGame();

	public void start(Stage stage) {
		Group root = new Group();
        VBox vbox = new VBox();
        HBox buttons = new HBox();
        HBox textFields = new HBox();

        Button bRock = new Button("rock");
        Button bPaper = new Button("paper");
        Button bScissor = new Button("scissor");

        TextField message = new TextField("Welcome!");
        message.setPrefWidth(200);
        TextField win = new TextField("Wins: ");
        TextField loss = new TextField("Losses: ");
        TextField tie = new TextField("Ties: ");

        RpsChoice rockChoice = new RpsChoice(win, loss, tie, message, "rock", this.game);
        bRock.setOnAction(rockChoice);
        RpsChoice paperChoice = new RpsChoice(win, loss, tie, message, "paper", this.game);
        bPaper.setOnAction(paperChoice);
        RpsChoice scissorChoice = new RpsChoice(win, loss, tie, message, "scissor", this.game);
        bScissor.setOnAction(scissorChoice);

        buttons.getChildren().addAll(bRock, bPaper, bScissor);
        textFields.getChildren().addAll(message, win, loss, tie);

        vbox.getChildren().addAll(buttons, textFields);
        root.getChildren().add(vbox);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
