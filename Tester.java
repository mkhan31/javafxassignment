// Mohammad Khan, 2038700
import java.util.Scanner;

public class Tester
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        RpsGame game = new RpsGame();
        while(true)
        {
            System.out.println("Enter 'rock', 'paper', or 'scissor'; 'stop' to exit'");
            String player = input.nextLine();
            if(player.equals("stop"))
            {
                break;
            };
            System.out.println(game.playRound(player));
            System.out.println("Loss: " + game.getLoss() + "\nWin: " + game.getWin() + "\nTie: " + game.getTie());
        }
        input.close();
    }
}
