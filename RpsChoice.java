import javax.swing.Action;

import javafx.event.*;
// Mohammad Khan, 2038700
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>
{
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private TextField message;
    private String playerChoice;
    private RpsGame game;

    public RpsChoice(TextField w, TextField l, TextField t, TextField message, String player, RpsGame game)
    {
        this.wins = w;
        this.losses = l;
        this.ties = t;
        this.message = message;
        this.playerChoice = player;
        this.game = game;
    }

    public void handle(ActionEvent e)
    {
        String playing = game.playRound(this.playerChoice);
        message.setText(playing);
        this.wins.setText("Wins: " + game.getWin());
        this.losses.setText("Losses: " + game.getLoss());
        this.ties.setText("Ties: " + game.getTie());
    }
} 