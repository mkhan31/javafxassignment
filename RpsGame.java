// Mohammad Khan, 2038700
import java.util.Random;

public class RpsGame
{
    private int win;
    private int loss;
    private int tie;
    private Random rand = new Random();

    public String playRound(String playerChoice)
    {
        String compChoice = "";
        int generatedInt = rand.nextInt(3);
        switch(generatedInt)
        {
            case 0:
                compChoice = "rock";
                break;
            case 1:
                compChoice = "paper";
                break;
            case 2:
                compChoice = "scissor";
                break;
        }
        if(compChoice.equals(playerChoice))
        {
            this.tie++;
            return "Computer chose " + compChoice + ", tie!";
        }
        else if(
        (playerChoice.equals("rock") && compChoice.equals("scissor"))
        || (playerChoice.equals("paper") && compChoice.equals("rock"))
        || (playerChoice.equals("scissor") && compChoice.equals("paper"))
        )
        {
            this.win++;
            return "Computer chose " + compChoice + ", you win!";
        }
        else
        {
            this.loss++;
            return "Computer chose " + compChoice + ", you lost!";
        }
    }

    public int getWin() {
        return this.win;
    }

    public int getLoss() {
        return this.loss;
    }

    public int getTie() {
        return this.tie;
    }
}
